
# API Automation

Test Automation framework is built to test out WebEngage REST APIs. Started out as a pilot project of QA Automation, the implementation of which has been integrated into Dashboard Automation as an utility. The framework is made up of Regression suite which is used to perform sanity checks on WebEngage REST APIs and Delete suite which is a helper utility  used for periodic data clearance from Test Accounts.

## Requirements
In order to setup this project you'll need following things installed and configured on your local machine
- Maven
- Java
- Java IDE
    - Cucumber Plugin
    - JSON Editor Plugin
    - Jenkins Editor Plugin
- Microsoft Excel

You'll need Office365 account if you want to add/update API endpoints or payloads.

## Tech Stack

- Cucumber
- Serenity
- Rest Assured
- JUnit
- Apache POI
- JSON-simple
- Gson
- Log4j


## Test Suites
Tests have been categorized into various suites to give user flexibility to choose test type. Also, will help in framework maintenanace and test readability. The Project Credentials of test account has been defined in [credentials.json](./src/test/resources/credentials.json). 
Goals and capabilities of each suite has been described below


### Regression Test Suite

Regression tests get executed on the test account. These tags are tagged at feature level. The tests having following tags along with `@Regression` tags get excluded from execution.
They essentially validate all the API endpoints. The detailed Request payloads and Responses are logged in 

Visit following [link](https://www.confluence.com "Regression Test Scenarios") to get a list of all the Regression scenarios.

### Delete Suite

Delete suite is an helper utility suite built to clear out data from Test account that get generated during execution of Dashboard Automation and API Automation suites. It excludes test data and deletes rest of the stuff, the details of wwhich are defined in [excludeFromDelete.json](./src/test/resources/excludeFromDelete.json). 
The sequence of deletion has been defined in modules section. When the suite is executed the user is required to provide test account name in prompt. Accordingly, the framework will source project credentials for the same internally and start hitting DELETE method.

_**Be cautious while making selections in Delete Suite**_



## Capabilities

### Modules
The tests have been grouped further on the basis of dashboard sections they address. This has been termed as modules in the framework. You can select modules that you wish to include/exclude from suite execution cycle.
Modules enabled till date for Regression suite
- Segments
- Integration
- Campaigns

In addition to above modules there are additional pre requisite modules(features) in `dataProvider` folder that get executed prior to execution of actual features. The data provider has capability to generate test data at runtime for the actual features to make use of

Modules enabled till date for Delete suite
- Journeys
- WA Campaigns
- SMS Campaigns
- Email Campaigns
- WebPush Campaigns
- InLine Content
- Push Campaigns
- InApp Content
- OnSite Notif
- Segments
- Static Lists
- SSP
- ESP
- WSP



### Environments
The above suites can be executed on any of these environments
- Prod US
- Prod IN
- Prod Beta
- Staging
    - Default Namespace
    - `automation` Namespace
    - Custom Namespace (User needs to define namespace in prompt)

## Usage

You can debug this project as JUnit Test by pointing to TestRunner.java.
But, this won't generate Test Reports and will also bypass Test Initalizer configuration.

To achive the above you need to execute this as a maven project
```bash
  mvn mvn clean compiler:compile
```

```bash
  mvn exec:java -Dset.Environment="Prod US" -Dset.Tag="Regression" -Dset.Module="All" -Dset.Namespace=automation -Dset.Account="UIAutomation" verify 
```
## Execution

Team has configured CICD pipeline to execute builds daily on dedicated Windows Machine. 
The schedule of builds are as following

|    Suite   	| Environment 	| Day of Week 	|   Time  	|
|:----------:	|:-----------:	|:-----------:	|:-------:	|
| Regression 	| Prod US     	| Everyday    	| 0800hrs 	|
| Delete 	| Prod IN     	| Wednesday    	| 0510hrs 	|
| Delete 	| Staging     	| Tuesday   	| 0515hrs 	|
| Delete 	| Prod US     	| Wednesday    	| 0510hrs 	|

User can also trigger build by passing parameter directly via Jenkins. User will be given an option to choose the machine to execute the suite on (Jenkins node), the default selection is Windows instance.

[Click here](https://jenkins.stg.webengage.biz/job/API%20Automation/build?delay=0sec "Trigger API Automation") to Trigger the Pipeline


## Reporting

The builds that get triggered or are explicitly triggered by the User get reported to [#testautomation](https://webklipper.slack.com/archives/C02AZHRRBLJ "Alerts channel") channel with all metadata of the Job and have the report links and build details embedded.
Depending on Suite that is executed the Slack thread varies. For ceratins suites, you'll see more results in the thread.
Please refer to Serenity reports to check the Request payloads, endpoints and response in details. All API metdata is populated in Serenity Reports for each and ecery API call.
## Documentation

Refer to following docs to get more info about the project

[Architecture](https://webengage.atlassian.net/wiki/spaces/QA/pages/2911109121/API+Automation+Framework+v0.8+August+2021)

[Latest Release Notes](https://repo.webengage.com/qa/api-automation/-/releases)

[Releases](https://linktodocumentation)

[Jenkins Pipeline](https://jenkins.stg.webengage.biz/job/API%20Automation/)