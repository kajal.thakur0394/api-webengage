package com.webengage.api_automation.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.webengage.api_automation.utilities.DatabaseUtility;

public class RelaysDAO {
	Connection con;
	BaseDAO baseD = new BaseDAO();

	public RelaysDAO() {
		try {
			con = DatabaseUtility.getConnection("we_testexecution");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<HashMap<Object, String>> getRelayExecutionTable() throws SQLException {
		String buildNo = System.getProperty("set.BuildNumber");
		ResultSet rs = DatabaseUtility.executeQuery(con,
				"select * from journeys_execution where build_number=" + buildNo + " and feature = 'Relays' and journey_id is not null");
		List<HashMap<Object, String>> resultSet = baseD.getResultSetObj(rs);
		return resultSet;
	}
}