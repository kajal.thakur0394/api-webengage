package com.webengage.api_automation.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.webengage.api_automation.utilities.DatabaseUtility;

public class StaticSegmentsDAO {
	Connection con;
	BaseDAO baseD = new BaseDAO();

	public StaticSegmentsDAO() {
		try {
			con = DatabaseUtility.getConnection("we_testexecution");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<HashMap<Object, String>> getSegmentExecutionTable() throws SQLException {
		String buildNo = System.getProperty("set.BuildNumber");
		ResultSet rs = DatabaseUtility.executeQuery(con,
				"select * from segment_execution where build_number=" + buildNo +" and scenario_name like '%static segment%' and segment_id is not null");
		List<HashMap<Object, String>> resultSet = baseD.getResultSetObj(rs);
		return resultSet;
	}
}