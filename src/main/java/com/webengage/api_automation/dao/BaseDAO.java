package com.webengage.api_automation.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BaseDAO {
	ResultSetMetaData metaData = null;

	public List<HashMap<Object, String>> getResultSetObj(ResultSet rs) throws SQLException {
		ResultSetMetaData metaData = rs.getMetaData();
		this.metaData = metaData;
		List<HashMap<Object, String>> data = new ArrayList<>();
		List<Object> nameList = new ArrayList<>();
		int uidIndex = getColumnIndex("user_id");
		int scnIndex = getColumnIndex("scenario");
		int featureIndex = getColumnIndex("feature");
		while (rs.next()) {
			String orgName = rs.getObject(scnIndex).toString();
			if (!nameList.contains(orgName)) {
				nameList.add(orgName);
				HashMap<Object, String> row = new HashMap<Object, String>();
				try {
					row.put("id", rs.getObject(2).toString());
				} catch (NullPointerException e) {
					System.out.println("id has null value");
					row.put("id", "null");
				}
				row.put("scenario_name", rs.getObject(scnIndex).toString());
				row.put("user_id", (String) rs.getObject(uidIndex).toString());
				try {
					row.put("expected_stats",
							(String) rs.getObject(getColumnIndex("expected_blockid_stats")).toString());
				} catch (SQLException e) {
					e.printStackTrace();
				}
				data.add(row);
			} else {
				for (int i = 0; i < data.size(); i++) {
					HashMap<Object, String> innerMap = data.get(i);
					String scnName = innerMap.get("scenario_name");
					if (scnName.equals(orgName)
							&& !(innerMap.get("user_id").toString().contains(rs.getObject(uidIndex).toString()))) {
						String user = innerMap.get("user_id").toString() + ";";
						innerMap.put("user_id", user + rs.getObject(uidIndex).toString());
						break;
					}
				}
			}
		}
		return data;
	}

	private int getColumnIndex(String colName) throws SQLException {
		int columnCount = metaData.getColumnCount();
		int index = 0;
		for (int i = 1; i <= columnCount; i++) {
			if (metaData.getColumnName(i).toString().contains(colName))
				index = i;
		}
		return index;
	}
}
