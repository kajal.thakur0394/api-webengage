package com.webengage.api_automation.dataProviders;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;

import com.webengage.api_automation.dao.EmailDAO;
import com.webengage.api_automation.dao.JourneysDAO;
import com.webengage.api_automation.dao.PushDAO;
import com.webengage.api_automation.dao.RelaysDAO;
import com.webengage.api_automation.dao.SegmentsDAO;
import com.webengage.api_automation.dao.SmsDAO;
import com.webengage.api_automation.dao.StaticSegmentsDAO;
import com.webengage.api_automation.dao.WebPushDAO;
import com.webengage.api_automation.dao.WhatsappDAO;

public class PayLoadDP {

	@DataProvider(name = "SegmentsData")
	public Iterator<HashMap<Object, String>> stringdataSegments() {
		System.out.println("DataProvider Segments");
		List<HashMap<Object, String>> resultSet = null;
		try {
			SegmentsDAO segDao = new SegmentsDAO();
			resultSet = segDao.getSegmentExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}

	@DataProvider(name = "SmsData")
	public Iterator<HashMap<Object, String>> stringdataSms() {
		System.out.println("DataProvider SMS");
		List<HashMap<Object, String>> resultSet = null;
		try {
			SmsDAO smsDao = new SmsDAO();
			resultSet = smsDao.getCampaignExecutionTable();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}

	@DataProvider(name = "EmailData")
	public Iterator<HashMap<Object, String>> stringdataEmail() {
		System.out.println("DataProvider Email");
		List<HashMap<Object, String>> resultSet = null;
		try {
			EmailDAO emaiDao = new EmailDAO();
			resultSet = emaiDao.getCampaignExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}

	@DataProvider(name = "WhatsappData")
	public Iterator<HashMap<Object, String>> stringdataWhatsapp() {
		System.out.println("DataProvider WhatsApp");
		List<HashMap<Object, String>> resultSet = null;
		try {
			WhatsappDAO wpDao = new WhatsappDAO();
			resultSet = wpDao.getCampaignExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}

	@DataProvider(name = "WebPushData")
	public Iterator<HashMap<Object, String>> stringdataWebPush() {
		System.out.println("DataProvider WebPush");
		List<HashMap<Object, String>> resultSet = null;
		try {
			WebPushDAO webPushDao = new WebPushDAO();
			resultSet = webPushDao.getCampaignExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}

	@DataProvider(name = "PushData")
	public Iterator<HashMap<Object, String>> stringdataPush() {
		System.out.println("DataProvider Push");
		List<HashMap<Object, String>> resultSet = null;
		try {
			PushDAO pushDao = new PushDAO();
			resultSet = pushDao.getCampaignExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}

	@DataProvider(name = "StaticSegmentsData")
	public Iterator<HashMap<Object, String>> stringdataStaticSegments() {
		System.out.println("DataProvider Static Segments");
		List<HashMap<Object, String>> resultSet = null;
		try {
			StaticSegmentsDAO segDao = new StaticSegmentsDAO();
			resultSet = segDao.getSegmentExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}

	@DataProvider(name = "JourneyData")
	public Iterator<HashMap<Object, String>> stringdataJourney() {
		List<HashMap<Object, String>> resultSet = null;
		try {
			JourneysDAO journeysDao = new JourneysDAO();
			resultSet = journeysDao.getJourneyExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}
	
	@DataProvider(name = "RelayData")
	public Iterator<HashMap<Object, String>> stringdataRelay() {
		List<HashMap<Object, String>> resultSet = null;
		try {
			 RelaysDAO relayDao = new RelaysDAO();
			resultSet = relayDao.getRelayExecutionTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}
	
}