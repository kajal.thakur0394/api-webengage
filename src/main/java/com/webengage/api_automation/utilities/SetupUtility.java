package com.webengage.api_automation.utilities;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SetupUtility {
	private static String authApiKey;
	private static String licenseCode;
	private String apiUri;
	public static final String directoryLoc = System.getProperty("user.dir");
	final String testDataexcel = directoryLoc + "/src/test/resources/testData/testData.xlsx";
	final String apiFilePath = directoryLoc + "/src/test/resources/testData/api.properties";
	final String credentials = directoryLoc + "/src/test/resources/testData/credentials.json";
	final static String postJSONBody = directoryLoc + "/src/test/resources/testData/jsonSchemas/";
	public static String cucumberPropPath = SetupUtility.directoryLoc
			+ "/src/test/resources/testData/buildCucumberOptions.properties";

	Properties prop = new Properties();

	public static String getAuthApiKey() {
		return authApiKey;
	}

	public void setAuthApiKey(String authApiKey) throws IOException {
		SetupUtility.authApiKey = authApiKey;
	}

	public String getLicenseCode() {
		return licenseCode;
	}

	public void setLicenseCode(String licenseCode) throws IOException {
		SetupUtility.licenseCode = licenseCode;
	}

	public String getApiURI() {
		return apiUri;
	}

	public void setApiURI(String apiName) throws IOException {
		FileInputStream apiFile = new FileInputStream(apiFilePath);
		prop.load(apiFile);
		this.apiUri = (prop.getProperty(apiName)).replace("{license}", getLicenseCode());
	}

	public String getBaseURI() throws IOException {
		FileInputStream apiFile = new FileInputStream(apiFilePath);
		prop.load(apiFile);
		switch (System.getProperty("set.Environment")) {
		case "Prod US":
			return prop.getProperty("prodUS");
		case "Prod IN":
			return prop.getProperty("prodIN");
		case "Prod KSA":
			return prop.getProperty("prodKSA");
		case "Prod Beta":
			return prop.getProperty("prodBeta");
		case "Staging":
			String staging = prop.getProperty("Staging");
			String namespace = System.getProperty("set.Namespace");
			return staging.replace("{namespace}", namespace);
		case "Staging Dev":
			return prop.getProperty("StagingDev");
		default:
			break;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public void loadCredentials(String projectName) throws IOException, ParseException {
		Object credObj = new JSONParser().parse(new FileReader(credentials));
		JSONObject jsonCredObj = (JSONObject) credObj;
		JSONObject envSpecific = (JSONObject) jsonCredObj.get(System.getProperty("set.Environment"));
		Map<String, String> projectCredentials = ((Map<String, String>) envSpecific.get(projectName));
		for (Map.Entry<String, String> indivEntry : projectCredentials.entrySet()) {
			if (indivEntry.getKey().equals("authorizationKey"))
				setAuthApiKey(indivEntry.getValue());
			else
				setLicenseCode(indivEntry.getValue());
		}
	}

	@SuppressWarnings("resource")
	public XSSFSheet readFromExcelSheet(String sheetName) throws IOException {
		FileInputStream fis = new FileInputStream(testDataexcel);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		return sheet;
	}

}
