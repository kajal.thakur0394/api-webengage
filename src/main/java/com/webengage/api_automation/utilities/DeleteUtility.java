package com.webengage.api_automation.utilities;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

public class DeleteUtility {
	SetupUtility setupUtility = new SetupUtility();
	CommonUtility commonUtility = new CommonUtility();
	Response response;
	ArrayList<String> idList;
	ArrayList<String> SSPId;
	ArrayList<String> excludeList;
	List<HashMap<String, String>> idCredList;
	final String excludeListFile = SetupUtility.directoryLoc + "/src/test/resources/testData/excludeFromDelete.json";

	public List<HashMap<String, String>> getIdCredList() {
		return idCredList;
	}

	public void setIdCredList(List<HashMap<String, String>> idCredList) {
		this.idCredList = idCredList;
	}

	public ArrayList<String> getExcludeList() {
		return excludeList;
	}

	public void setExcludeList(ArrayList<String> excludeList) {
		this.excludeList = excludeList;
	}

	public ArrayList<String> getListOfId() {
		return idList;
	}

	public void setListOfId(ArrayList<String> idList) {
		this.idList = idList;
	}

	public ArrayList<String> getSPId() {
		return SSPId;
	}

	public void setSPId(ArrayList<String> SSPId) {
		this.SSPId = SSPId;
	}

	@Step("API details")
	public void fetchAllIds(String api, String jsonPathforId) throws IOException {
		ArrayList<String> excludeList = getExcludeList();
		ArrayList<String> idList = new ArrayList<>();
		setupUtility.setApiURI(api);
		String apiURI = setupUtility.getApiURI() + "?pageNo=1&pageSize=10";
		String queryParams;
		if (api.contains("Push") || api.contains("InApp")) {
			queryParams = "&sdks=2,3";
		} else if (api.contains("OnSite")) {
			queryParams = "&sdks=1";
		} else if (api.contains("ContentApp")) {
			queryParams = "&sdks=";
		} else if (api.contains("Relays")) {
			queryParams = "&journeyType=RELAY";
		} else {
			queryParams = "";
		}
		commonUtility.getRequest(apiURI + queryParams);
		response = CommonUtility.response;
		int count = Integer
				.parseInt(JsonPath.from(response.body().asString()).get("response.data.totalCount").toString());
		int pages = count / 10;
		pages = (count % 10) != 0 ? ++pages : pages;
		for (int i = 1; i <= pages; i++) {
			apiURI = setupUtility.getApiURI() + "?pageNo=" + i + "&pageSize=10";
			commonUtility.getRequest(apiURI + queryParams);
			response = CommonUtility.response;
			int listSize = (i == pages && count % 10 == 0) ? 10 : (i == pages) ? (count % 10) : 10;
			for (int j = 0; j < listSize; j++) {
				String id;
				try {
					String jsonPathValue = api.contains("Funnels") ? "response.data.contents.funnel[" + j + "]."
							: "response.data.contents[" + j + "].";
					id = JsonPath.from(response.body().asString()).get(jsonPathValue + jsonPathforId).toString();
					String nameOrTitle = (api.contains("Campaigns") | api.contains("Funnels")) ? "title"
							: api.contains("CustomTemplate") ? "templateName" : "name";
					String value = JsonPath.from(response.body().asString()).get(jsonPathValue + nameOrTitle)
							.toString();
					if (excludeList.stream().anyMatch(s -> s.equals(value))) {
						excludeList.remove(new String(value));
						continue;
					}
					if (api.contains("Campaigns")) {
						String category = JsonPath.from(response.body().asString())
								.get("response.data.contents[" + j + "].category").toString();
						if (category.equals("journey")) {
							continue;
						}
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
					break;
				}
				idList.add(id);
			}
		}
		setListOfId(idList);
	}

	@Step("API details")
	public void deleteAllfromList(String api, String keywordToReplace) throws IOException {
		for (String s : getListOfId()) {
			setupUtility.setApiURI(api);
			String apiURI = setupUtility.getApiURI().replace(keywordToReplace, s);
			try {
				commonUtility.deleteRequest(apiURI);
			} catch (AssertionError e) {
				Assert.assertNotEquals(500, CommonUtility.response.getStatusCode());
			}
		}
	}

	@Step("API details")
	public void fetchAllSPId(String channelSP) throws IOException {
		ArrayList<String> excludeList = getExcludeList();
		ArrayList<String> sspIdList = new ArrayList<>();
		setupUtility.setApiURI("verify" + channelSP + "SP");
		String apiURI = setupUtility.getApiURI();
		commonUtility.getRequest(apiURI);
		response = CommonUtility.response;
		for (int j = 0; j < response.jsonPath().getList("response.data").size(); j++) {
			try {
				String spId = JsonPath.from(response.body().asString()).get("response.data[" + j + "].id").toString();
				String name = JsonPath.from(response.body().asString()).get("response.data[" + j + "].name").toString();
				if (excludeList.stream().anyMatch(s -> s.equals(name))) {
					excludeList.remove(new String(name));
					continue;
				}
				sspIdList.add(spId);
			} catch (NullPointerException e) {
				break;
			}
		}
		setSPId(sspIdList);
	}

	@Step("API details")
	public void deleteFilteredSP(String channelSP) throws IOException {
		for (String s : getSPId()) {
			setupUtility.setApiURI("delete" + channelSP + "SP");
			String apiURI = setupUtility.getApiURI().replace("{SPId}", s);
			try {
				commonUtility.deleteRequest(apiURI);
			} catch (AssertionError e) {
				Assert.assertNotEquals(500, CommonUtility.response.getStatusCode());
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void excludeFromListwithName(String type) {
		Object listObj = null;
		try {
			listObj = new JSONParser().parse(new FileReader(excludeListFile));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		JSONObject jsonCredObj = (JSONObject) listObj;
		JSONArray list = (JSONArray) jsonCredObj.get(type);
		setExcludeList(list);
	}

	public void fetchAllWSPTemplateIds(String api) throws IOException {
		ArrayList<String> excludeList = getExcludeList();
		List<HashMap<String, String>> idCredList = new ArrayList<>();
		setupUtility.setApiURI(api);
		String apiURI = setupUtility.getApiURI();
		String queryParams = "?pageSize=2000";
		commonUtility.getRequest(apiURI + queryParams);
		response = CommonUtility.response;
		for (int j = 0; j < response.jsonPath().getList("response.data.contents").size(); j++) {
			HashMap<String, String> idAndCredsMap = new HashMap<>();
			try {
				String wspCredentialId = JsonPath.from(response.body().asString())
						.get("response.data.contents[" + j + "].wspCredentialId").toString();
				String templateId = JsonPath.from(response.body().asString())
						.get("response.data.contents[" + j + "].id").toString();
				String templateName = JsonPath.from(response.body().asString())
						.get("response.data.contents[" + j + "].templateJson.templateName").toString();
				if (excludeList.stream().anyMatch(s -> s.equals(templateName))) {
					excludeList.remove(new String(templateName));
					continue;
				}
				idAndCredsMap.put("wspCredentialId", wspCredentialId);
				idAndCredsMap.put("templateId", templateId);
				idCredList.add(idAndCredsMap);
			} catch (NullPointerException e) {
				break;
			}
		}
		setIdCredList(idCredList);
	}

	public void deleteAllTemplatesFromList(String api) throws IOException {
		for (HashMap<String, String> s : getIdCredList()) {
			String wspCredentialId = s.get("wspCredentialId");
			String templateId = s.get("templateId");
			setupUtility.setApiURI(api);
			String apiURI = setupUtility.getApiURI().replace("{wspCredentialId}", wspCredentialId)
					.replace("{templateId}", templateId);
			try {
				commonUtility.deleteRequest(apiURI);
			} catch (AssertionError e) {
				Assert.assertNotEquals(500, CommonUtility.response.getStatusCode());
			}
		}
	}
}