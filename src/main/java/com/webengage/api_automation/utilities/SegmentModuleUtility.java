package com.webengage.api_automation.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

public class SegmentModuleUtility {
	public static LinkedHashMap<String, String> runtimeURL = new LinkedHashMap<>();
	public static LinkedHashMap<String, String> runtimeBody = new LinkedHashMap<>();
	LinkedList<LinkedList<String>> runtimeQueryParams = new LinkedList<>();

	public void setDynamicRequest(String runtimeKey, String type) {
		LinkedList<String> runtimeList = new LinkedList<>();
		String value = CommonUtility.runtimeValues.get(runtimeKey);
		String replaceWord="";
		if(runtimeKey.contains("."))
		{
			replaceWord = "{" + runtimeKey.split("\\.")[1] + "}";
			replaceWord = replaceWord.replaceAll("[0-9]", "");
		}
		else
			replaceWord="{"+runtimeKey+"}";
		runtimeList.add(replaceWord);
		runtimeList.add(value);
		switch (type) {
		case "URL":
			runtimeURL.put(replaceWord, value);
			break;
		case "Body":
			runtimeBody.put(replaceWord, value);
			break;
		case "Query":
			runtimeQueryParams.add(runtimeList);
			break;
		default:
			break;
		}
	}

	public LinkedHashMap<String, String> getDynamicURL() {
		return runtimeURL;
	}

	public LinkedHashMap<String, String> getDynamicBody() {
		return runtimeBody;
	}

	public LinkedList<LinkedList<String>> getDynamicQueryParams() {
		return runtimeQueryParams;
	}

	public void setDynamicRequestLicenseCode(String type, String value) {
		LinkedList<String> runtimeList = new LinkedList<>();
		runtimeList.add("{licenseCode}");
		runtimeList.add(value);
		switch (type) {
		case "URL":
			runtimeURL.put("{licenseCode}", value);
			break;
		case "Body":
			runtimeBody.put("{licenseCode}", value);
			break;
		case "Query":
			runtimeQueryParams.add(runtimeList);
			break;
		default:
			break;
		}

	}

	public String modifyAPIURL(String apiURI) {
		if (getDynamicURL() != null) {
			for (Entry<String, String> element : getDynamicURL().entrySet()) {
				String replaceKeyword = element.getKey();
				String value = element.getValue();
				apiURI = apiURI.replace(replaceKeyword, value);
			}
			return apiURI;
		} else {
			return apiURI;
		}
	}

	public String modifyBody(String jsonBody) {
		if (getDynamicBody() != null) {
			for (Entry<String, String> element : getDynamicBody().entrySet()) {
				String replaceKeyword = element.getKey();
				String value = element.getValue();
				try {
					jsonBody = jsonBody.replace(replaceKeyword, value);
				} catch (Exception e) {
					System.out.println("There is no replacement value present for "+replaceKeyword);
				}
				
			}
			return jsonBody;
		} else {
			return jsonBody;
		}
	}

	public String modifyQueryParams(String queryParamsString) {
		if (getDynamicQueryParams() != null) {
			for (LinkedList<String> list : getDynamicQueryParams()) {
				String replaceKeyword = list.get(0);
				String value = list.get(1);
				queryParamsString = queryParamsString.replace(replaceKeyword, value);
			}
			return queryParamsString;
		} else {
			return queryParamsString;
		}
	}
	
	public void appendFromAndToDate() {
		createFromDate();
		createToDate();
	}

	private void createFromDate() {
		CommonUtility.runtimeValues.put("from", createDate().concat("00:00:00.000%2B05:30"));
	}

	private void createToDate() {
		CommonUtility.runtimeValues.put("to", createDate().concat("23:59:59.999%2B05:30"));
	}

	private String createDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'");
		Date date = new Date();
		return formatter.format(date).toString();
	}

}