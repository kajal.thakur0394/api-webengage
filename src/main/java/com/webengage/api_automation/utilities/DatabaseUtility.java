package com.webengage.api_automation.utilities;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.*;
import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class DatabaseUtility {
    public static final String directoryLoc = System.getProperty("user.dir") + "/src/test/resources/testData/";
    public static final String sqlFilePath = directoryLoc + "SQLScripts/";
    public static Properties dataSourceProp;
    private final static Map<String, DataSource> DB_POOLMAP = new ConcurrentHashMap<>();

    public static synchronized Connection getConnection(String dbName) throws Exception {
        if (!DB_POOLMAP.containsKey(dbName)) {
            createDataSource(dbName);
            System.out.println("Connection Formed");
        }
        DataSource dataSource = DB_POOLMAP.get(dbName);
        return dataSource.getConnection();
    }

    private static void createDataSource(String dbName) throws Exception {
        ////Log.info("Creating the datasource for " + dbName);
        HikariConfig hikariConfig = getHikariConfig(dbName);
        HikariDataSource hikariDataSource = new HikariDataSource(hikariConfig);

        ////Log.info("Adding the datasource to the datasouce map");
        DB_POOLMAP.put(dbName, hikariDataSource);
    }

    private static HikariConfig getHikariConfig(String dbName) throws Exception {
        Properties properties = getDataSourceProp();
        if (properties == null || properties.get(dbName + ".jdbcUrl") == null) {
            throw new Exception("Database not defined");
        }

        HikariConfig hikariConfig = new HikariConfig();
        Function<String, String> getValue = (key) -> properties.get(dbName + "." + key).toString();

        hikariConfig.setJdbcUrl(getValue.apply("jdbcUrl"));
        hikariConfig.setUsername(getValue.apply("username"));
        hikariConfig.setPassword(getValue.apply("password"));
        hikariConfig.setPoolName(dbName + "_Pool");
        hikariConfig.addDataSourceProperty("cachePrepStmts", getValue.apply("cachePrepStmts"));
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", Integer.parseInt(getValue.apply("prepStmtCacheSize")));
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", Integer.parseInt(getValue.apply("prepStmtCacheSqlLimit")));
        hikariConfig.addDataSourceProperty("useServerPrepStmts", getValue.apply("useServerPrepStmts"));
        hikariConfig.addDataSourceProperty("useLocalSessionState", getValue.apply("useLocalSessionState"));
        hikariConfig.addDataSourceProperty("rewriteBatchedStatements", getValue.apply("rewriteBatchedStatements"));
        hikariConfig.addDataSourceProperty("cacheResultSetMetadata", getValue.apply("cacheResultSetMetadata"));
        hikariConfig.addDataSourceProperty("cacheServerConfiguration", getValue.apply("cacheServerConfiguration"));
        hikariConfig.addDataSourceProperty("elideSetAutoCommits", getValue.apply("elideSetAutoCommits"));
        hikariConfig.addDataSourceProperty("maintainTimeStats", getValue.apply("maintainTimeStats"));

        return hikariConfig;
    }

    private static Properties getDataSourceProp() {
        if (dataSourceProp != null) {
            return dataSourceProp;
        }

        ////Log.info("Loading the datasource configuration File");
        String propertiesFileName = "dataSource.properties";

        try (InputStream istream = DatabaseUtility.class.getClassLoader().getResourceAsStream(propertiesFileName)) {
            Properties properties = new Properties();
            properties.load(istream);
            dataSourceProp = properties;
            return properties;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void closeDataSources() {
        for (DataSource ds : DB_POOLMAP.values()) {
            ((HikariDataSource) ds).close();
        }
    }

    public static void executeUpdate(Connection connection, String sqlQuery) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        int updatedRows = preparedStatement.executeUpdate();

        if (updatedRows > 0)
        	System.out.println("Successfully updated/inserted. " + updatedRows + " row/rows affected.");
            //Log.info("Successfully updated/inserted. " + updatedRows + " row/rows affected.");
        else
        	System.out.println("Updation/Insertion failed");
            //Log.error("Updation/Insertion failed");
        	
    }

    public static ResultSet executeQuery(Connection connection, String sqlQuery) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet;
    	}
       

    public static ResultSetMetaData fetchTableInfo(Connection connection, String sqlQuery) throws SQLException {
        ResultSet rs = executeQuery(connection, sqlQuery);
        return rs.getMetaData();
    }

    public static ResultSet runSQLQueryScript(Connection connection, String sqlScriptFileName) {
        String sqlScript = null;
        try (FileReader sqlScriptFile = new FileReader(sqlFilePath + sqlScriptFileName);
             BufferedReader br = new BufferedReader(sqlScriptFile);
        ) {
            StringBuilder sb = new StringBuilder("");
            while ((sqlScript = br.readLine()) != null) {
                sb.append(sqlScript).append("\n");
            }
            PreparedStatement preparedStatement = connection.prepareStatement(sb.toString());

            boolean hasResultSet = preparedStatement.execute();
            if (hasResultSet) {
                //Log.info("Successfully executed SQL script " + sqlScriptFileName);
            } else {
                //Log.error("Check SQL script queries.");
            }
            return preparedStatement.getResultSet();

        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void runSQLUpdateScript(Connection connection, String sqlScriptFileName) {
        String sqlScript = null;
        try (FileReader sqlScriptFile = new FileReader(sqlFilePath + sqlScriptFileName);
             BufferedReader br = new BufferedReader(sqlScriptFile);
        ) {
            StringBuilder sb = new StringBuilder("");
            while ((sqlScript = br.readLine()) != null) {
                sb.append(sqlScript).append("\n");
            }
            PreparedStatement preparedStatement = connection.prepareStatement(sb.toString());
            boolean hasResultSet = preparedStatement.execute();
            if (hasResultSet) {
                //Log.error("Check SQL script queries.");
            } else {
                //Log.info("Successfully updated/inserted. " + preparedStatement.getUpdateCount() + " row/rows affected.");
            }
        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
