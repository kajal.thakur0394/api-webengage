package com.webengage.api_automation.utilities;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import com.webengage.api_automation.helperclasses.CustomLogFilter;
import com.webengage.api_automation.helperclasses.RestAssuredBuilder;
import io.restassured.filter.Filter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class CommonUtility {
	public static String jsonBody;
	static RequestSpecification getRequest;
	static String getapiuri;
	SetupUtility setupUtility = new SetupUtility();
	public static Response response;
	public static HashMap<String, String> runtimeValues = new HashMap<>();
	LinkedHashMap<String, String> selectedJsonObj = new LinkedHashMap<>();

	public Object methodInvocation(HashMap<String, HashMap<String, String>> data, String className, Object obj)
			throws Exception {
		HashMap<String, String> valueData = data.get(className);
		for (Map.Entry<String, String> me : valueData.entrySet()) {
			String methodName = me.getKey().toString().split("-")[0];
			String parameterDataType = me.getKey().toString().split("-")[1];
			String value = me.getValue().toString();
			Method methodInstance = null;
			switch (parameterDataType) {
			case "String":
				methodInstance = obj.getClass().getMethod(methodName, String.class);
				methodInstance.invoke(obj, value);
				break;
			case "Double":
				methodInstance = obj.getClass().getMethod(methodName, Double.class);
				methodInstance.invoke(obj, Double.parseDouble(value));
				break;
			case "Boolean":
				methodInstance = obj.getClass().getMethod(methodName, Boolean.class);
				methodInstance.invoke(obj, Boolean.parseBoolean(value));
				break;
			case "Integer":
				methodInstance = obj.getClass().getMethod(methodName, Integer.class);
				methodInstance.invoke(obj, (int) Double.parseDouble(value));
				break;
			default:
				break;
			}
		}
		return obj;
	}

	@Step("API details")
	public void postRequest(String jsonBody, List<List<String>> queryParams, String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "POST");
		if (jsonBody.equals("-"))
			response = restAssur.executeWithQueryParams(queryParams);
		else
			response = queryParams.get(0).get(0).equals("-") ? restAssur.executeWithBody(jsonBody)
					: restAssur.executeWithBodyAndQueryParams(jsonBody, queryParams);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void postRequest(String jsonBody, String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "POST");
		response = restAssur.executeWithBody(jsonBody);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void postRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "POST");
		response = (jsonBody == null) ? restAssur.execute() : restAssur.executeWithBody(jsonBody);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void getRequest(String apiURI, List<List<String>> queryParams) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "GET");
		response = restAssur.executeWithQueryParams(queryParams);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void getRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "GET");
		response = restAssur.execute();
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void deleteRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "DELETE");
		response = restAssur.execute();
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	public void verifyStatusCode(int actualResponseCode) {
		Assert.assertTrue("Expected Status code: 200 or 201. Actual Status code: " + actualResponseCode + ".",
				actualResponseCode == 200 || actualResponseCode == 201);
	}

	public void verifyValue(String jsonPath, String expectedValue) {
		// Implementation to handle different data types to be done in future
		String actualValue = JsonPath.from(response.body().asString()).get(jsonPath).toString();
		if (expectedValue.contains("/")) {
			boolean check = false;
			if (expectedValue.contains(actualValue)) {
				check = true;
			}
			Assert.assertTrue(check);
		} else {
			Assert.assertEquals(expectedValue, actualValue);
		}
	}

	public String fetchValue(String jsonPath) {
		return JsonPath.from(response.body().asString()).get(jsonPath);
	}

	@Step("API details")
	public void passQueryParams(List<List<String>> queryParams) {
		Filter logFilter = new CustomLogFilter();
		for (List<String> s : queryParams) {
			String key = s.get(0);
			String value = s.get(1).contains("\"") ? s.get(1).replaceAll("\"", "") : s.get(1);
			getRequest.queryParam(key, value);
		}
		response = getRequest.filter(logFilter).get(getapiuri);
		recordReportData();
	}

	private void recordReportData() {
		try {
			Serenity.recordReportData().withTitle("API Request").andContents(CustomLogFilter.logRequest);
			Serenity.recordReportData().withTitle("API Response").andContents(CustomLogFilter.logResponse);
		} catch (Exception e) {
			System.out.println("Response->" + response.body().asString());
		}
	}

	public void fetchStaticBody(String apiNameBody, String refId) throws IOException, ParseException {
		Object obj = new JSONParser().parse(apiNameBody);
		JSONObject jo = (JSONObject) obj;
		jsonBody = jo.toJSONString();
	}

	public void createJsonBody(String body) throws ParseException {
		Object obj = new JSONParser().parse(body);
		JSONObject jo = (JSONObject) obj;
		jsonBody = jo.toJSONString();
	}

	public List<List<String>> generateQueryParams(String queryParamString) {
		List<List<String>> queryParam = new ArrayList<>();
		List<String> indiv;
		String[] indivParam = queryParamString.split("\\R");
		for (String temp : indivParam) {
			indiv = new ArrayList<>();
			for (String values : temp.split(":", 2)) {
				indiv.add(values);
			}
			queryParam.add(indiv);
		}
		return queryParam;

	}

	@SuppressWarnings("unchecked")
	public void checkIfPresent(String jsonPath, String expectedValue) {
		boolean checkFlag = false;
		List<Object> actualValue = JsonPath.from(response.body().asString()).getList(jsonPath.split("\\|")[0]);
		for (int i = 0; i < actualValue.size(); i++) {
			LinkedHashMap<String, String> temp = (LinkedHashMap<String, String>) actualValue.get(i);
			if (temp.get(jsonPath.split("\\|")[1]).equals(expectedValue)) {
				selectedJsonObj = temp;
				checkFlag = true;
				break;
			}
		}
		Assert.assertTrue(checkFlag);
	}

	@Step("API details")
	public void putRequest(String jsonBody, List<List<String>> queryParams, String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "PUT");
		if (jsonBody.equals("-"))
			response = restAssur.executeWithQueryParams(queryParams);
		else
			response = queryParams.get(0).get(0).equals("-") ? restAssur.executeWithBody(jsonBody)
					: restAssur.executeWithBodyAndQueryParams(jsonBody, queryParams);
		Serenity.recordReportData().withTitle("API Request").andContents(CustomLogFilter.logRequest);
		Serenity.recordReportData().withTitle("API Response").andContents(CustomLogFilter.logResponse);
	}

	@Step("API details")
	public void putRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "PUT");
		response = restAssur.execute();
		Serenity.recordReportData().withTitle("API Request").andContents(CustomLogFilter.logRequest);
		Serenity.recordReportData().withTitle("API Response").andContents(CustomLogFilter.logResponse);
	}

	public void verifyFromJsonObj(String identifier, String value) {
		Assert.assertEquals(value, selectedJsonObj.get(identifier));
	}

	public void verifyValuefromArray(String jsonPath, String expectedValue) {
		boolean check = false;
		int count = 0;
		try {
			count = Integer
					.parseInt(JsonPath.from(response.body().asString()).get("response.data.totalCount").toString());

		} catch (NullPointerException e) {
			JsonPath jPath = JsonPath.from(response.body().asString());
			count = jPath.getList("response.data.stats").size();
		}
		for (int i = 0; i < count; i++) {
			String actualValue = JsonPath.from(response.body().asString()).get(jsonPath.replace("[i]", "[" + i + "]"))
					.toString();
			if (expectedValue.equalsIgnoreCase(actualValue)) {
				check = true;
				break;
			}
		}
		Assert.assertTrue(check);
	}

	public void verifyTotalUser(String jsonPath,String[] users) {
		Assert.assertTrue(Integer.parseInt(JsonPath.from(response.body().asString()).get(jsonPath).toString()) >= users.length);
	}
	
	public void verifyTotalUserCount(String jsonPath,String[] users) {
		Assert.assertEquals("total users count is not correct", users.length, Integer.parseInt(JsonPath.from(response.body().asString()).get(jsonPath).toString()));
	}

	public List<Object> fetchObjectList(String path) {
		JsonPath jsonPath = response.jsonPath();
		return jsonPath.get(path);
	}

	public JSONObject getJsonObject(String data) throws ParseException {
		Object obj = new JSONParser().parse(data);
		return (JSONObject) obj;
	}

	public String fetchJpathValue(String path) {
		JsonPath jsonPath = response.jsonPath();
		return jsonPath.getString(path);
	}

}
