package com.webengage.api_automation.utilities;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import com.google.gson.Gson;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

public class CampaignModuleUtility {
	SetupUtility setupUtility = new SetupUtility();
	CommonUtility commonUtility = new CommonUtility();
	Response response;
	int trafficSegmentsPages;
	int idLimit=300;

	@Step
	public void setDynamicRequestBody(String values) throws IOException {
		LinkedList<String> runtimeList = new LinkedList<>();
		String finalReplacement = "";
		String replaceWord = "{SegmentIds}";
		String[] valueArr = values.split(",");
		for (String value : valueArr) {
			if (value.contains(".")) {
				String tempValue = CommonUtility.runtimeValues.get(value.trim());
				finalReplacement = finalReplacement.concat("\"" + tempValue + "\",");
			} else {
				getTrafficSegmentsPage();
				String tempValue = getSegmentIdfromSegName(value.trim());
				finalReplacement = finalReplacement.concat("\"" + tempValue + "\",");
			}
		}
		runtimeList.add(replaceWord);
		runtimeList.add(finalReplacement.substring(0, finalReplacement.length() - 1));
		SegmentModuleUtility.runtimeBody.put(replaceWord, finalReplacement.substring(0, finalReplacement.length() - 1));
	}

	private String getSegmentIdfromSegName(String value) throws IOException {
		String segmentId = null;
		for (int i = 1; i <= trafficSegmentsPages; i++) {
			String apiURI = setupUtility.getApiURI() + "?pageNo=" + i + "&pageSize=10";
			commonUtility.getRequest(apiURI);
			response = CommonUtility.response;
			for (int j = 0; j < 10; j++) {
				String segmentName;
				try {
					segmentName = JsonPath.from(response.body().asString())
							.get("response.data.contents[" + j + "].name").toString();
					if (segmentName.equals(value.trim()))
						segmentId = JsonPath.from(response.body().asString())
								.get("response.data.contents[" + j + "].id").toString();
				} catch (NullPointerException e) {
					break;
				}
			}
		}
		return segmentId;
	}

	public void getTrafficSegmentsPage() throws IOException {
		setupUtility.setApiURI("trafficSegments");
		String apiURI = setupUtility.getApiURI() + "?pageNo=1&pageSize=10";
		commonUtility.getRequest(apiURI);
		response = CommonUtility.response;
		int count = Integer
				.parseInt(JsonPath.from(response.body().asString()).get("response.data.totalCount").toString());
		int pages = count / 10;
		trafficSegmentsPages = (count % 10) != 0 ? ++pages : pages;
	}

	public void setDynamicRequestBodyforSSP(String value) throws IOException {
		LinkedList<String> runtimeList = new LinkedList<>();
		String finalReplacement = "";
		String replaceWord = "{SSPId}";
		if (value.contains(".")) {
			finalReplacement = CommonUtility.runtimeValues.get(value);
		} else {
			finalReplacement = getSSPIdfromSSPName(value);
		}
		runtimeList.add(replaceWord);
		runtimeList.add(finalReplacement);
		SegmentModuleUtility.runtimeBody.put(replaceWord, finalReplacement);
	}

	private String getSSPIdfromSSPName(String value) throws IOException {
		setupUtility.setApiURI("verifySSP");
		String apiURI = setupUtility.getApiURI();
		commonUtility.getRequest(apiURI);
		String SSPid = null;
		response = CommonUtility.response;
		for (int j = 0; j < idLimit; j++) {
			String name = JsonPath.from(response.body().asString()).get("response.data[" + j + "].name").toString();
			if (name.equals(value.trim())) {
				SSPid = JsonPath.from(response.body().asString()).get("response.data[" + j + "].id").toString();
				break;
			}
		}
		return SSPid;
	}

	private String getAndroidIdfromPackageName(String value) throws IOException {
		setupUtility.setApiURI("fetchAndroidConfig");
		String apiURI = setupUtility.getApiURI();
		commonUtility.getRequest(apiURI);
		String androidId = null;
		response = CommonUtility.response;
		for (int j = 0; j < idLimit; j++) {
			String name = JsonPath.from(response.body().asString()).get("response.data.contents[" + j + "].packageName")
					.toString();
			if (name.equals(value.trim())) {
				androidId = JsonPath.from(response.body().asString()).get("response.data.contents[" + j + "].id").toString();
				break;
			}
		}
		return androidId;
	}

	private String getiosIdfromBundleName(String value) throws IOException {
		setupUtility.setApiURI("fetchiOSConfig");
		String apiURI = setupUtility.getApiURI();
		commonUtility.getRequest(apiURI);
		String iosId = null;
		response = CommonUtility.response;
		for (int j = 0; j < idLimit; j++) {
			String name = JsonPath.from(response.body().asString()).get("response.data.contents[" + j + "].bundleName")
					.toString();
			if (name.equals(value.trim())) {
				iosId = JsonPath.from(response.body().asString()).get("response.data.contents[" + j + "].id").toString();
				break;
			}
		}
		return iosId;
	}

	@Step
	@SuppressWarnings("unchecked")
	public void setAppIds(String packageName, String bundleName) throws IOException {
		JSONObject appids = new JSONObject();
		JSONObject indivId;
		JSONArray andoidArr = new JSONArray();
		JSONArray iosArr = new JSONArray();
		String sdks = "";

		if (!packageName.isEmpty()) {
			indivId = new JSONObject();
			indivId.put("id", getAndroidIdfromPackageName(packageName));
			andoidArr.add(indivId);
			appids.put("ANDROID", andoidArr);
			sdks = sdks.concat("2,");
		}
		if (!bundleName.isEmpty()) {
			indivId = new JSONObject();
			indivId.put("id", getiosIdfromBundleName(bundleName));
			iosArr.add(indivId);
			appids.put("IOS", iosArr);
			sdks = sdks.concat("3,");
		}
		SegmentModuleUtility.runtimeBody.put("{appIds}", appids.toJSONString());
		SegmentModuleUtility.runtimeBody.put("{sdks}", sdks.substring(0, sdks.length() - 1));
	}

	public void setEpochTime(String time) {
		long unixTime = 0;
		if (time.equals("current time")) {
			Date date = new Date();
			unixTime = date.getTime() / 1000L;
		}
		SegmentModuleUtility.runtimeBody.put("{startDate}", String.valueOf(unixTime));
	}

	public void setTemplateCredential(String templateName) {
		try {
			HashMap<String, String> values=getTemplateAndWSPId(templateName);
			SegmentModuleUtility.runtimeBody.put("{credentialsId}", values.get("credentialsId"));
			SegmentModuleUtility.runtimeBody.put("{wspId}", values.get("credentialsId"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private HashMap<String, String> getTemplateAndWSPId(String templateName) throws IOException {
		setupUtility.setApiURI("fetchWATemplate");
		String apiURI = setupUtility.getApiURI();
		commonUtility.getRequest(apiURI);
		HashMap<String, String> values=new HashMap<>();
		response = CommonUtility.response;
		for (int j = 0; j < idLimit; j++) {
			String name = JsonPath.from(response.body().asString())
					.get("response.data.contents[" + j + "].templateJson.templateName").toString();
			if (name.equals(templateName.trim())) {
				String credentialsID = JsonPath.from(response.body().asString()).get("response.data.contents[" + j + "].wspCredentialId").toString();
				String wspID=JsonPath.from(response.body().asString()).get("response.data.contents[" + j + "].id").toString();
				values.put("credentialsId", credentialsID);
				values.put("wspId", wspID);
				break;
			}
		}
		return values;
	}

	public void fetchStatsList(String path, HashMap<String, String> paraMap) throws ParseException {
		List<Object> objectList = null;
		int count = 0;
		objectList = commonUtility.fetchObjectList(path);
		for (int i = 0; i < objectList.size(); i++) {
			String jsonInString = new Gson().toJson(objectList.get(i));
			JSONObject jo;
			jo = commonUtility.getJsonObject(jsonInString);
			if (jo.containsKey("name") && jo.containsKey("groupFunction")
					&& jo.get("groupFunction").toString().equalsIgnoreCase("COUNT")
					&& paraMap.keySet().contains(jo.get("name").toString())) {
				String jsonPath = "response.data[0].dimensions[0].metrics" + "[" + i + "].value";
				String value = paraMap.get(jo.get("name").toString());
				String responseValue = commonUtility.fetchJpathValue(jsonPath);
				if (value.equals(responseValue))
					count++;
			}
		}
		Assert.assertTrue("Stats not matched", paraMap.keySet().size() == count);
	}
}
