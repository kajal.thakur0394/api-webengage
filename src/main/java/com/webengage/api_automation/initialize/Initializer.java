package com.webengage.api_automation.initialize;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import com.webengage.api_automation.utilities.SetupUtility;

public class Initializer {
	public static String dataPath = "src/test/resources/features/dataProvider/";
	public static String modulesPath = "src/test/resources/features/modules/";
	public static String dataFolder = System.getProperty("user.dir") + "/" + dataPath;
	public static String modulesFolder = System.getProperty("user.dir") + "/" + modulesPath;

	public static void main(String[] args) throws IOException {
		buildCucumberProps();
	}

	public static void buildCucumberProps() throws IOException {
		Properties cucumberOptProps = new Properties();
		String suite = System.getProperty("set.Suite");
		String module = System.getProperty("set.Module");
		switch (suite) {
		case "Regression":
			cucumberOptProps = getRegressionProperties(module);
			break;
		case "Sanity":
			cucumberOptProps = getSanityProperties(module);
			break;
		case "DataProvider":
			cucumberOptProps = getDataProviderProperties(module);
			break;
		case "Delete":
			cucumberOptProps = getDeleteProperties(module);
			break;
		case "Smoke":
			cucumberOptProps = getSmokeProperties();
			break;
		default:
			break;
		}
		FileOutputStream outputStream = new FileOutputStream(SetupUtility.cucumberPropPath);
		cucumberOptProps.store(outputStream, "Cucumber Options for current build");

	}

	private static Properties getDataProviderProperties(String module) {
		Properties regProp = new Properties();
		regProp.put("cucumber.filter.tags", "@DataProvider");
		regProp.put("cucumber.features",
				"src/test/resources/features/dataProvider,src/test/resources/features/modules");
		return regProp;
	}

	public static String dataProvFeatures(String module) {
		StringBuilder dataProvStringBuilder = new StringBuilder();
		String[] featureNames = new File(dataFolder).list();
		Arrays.sort(featureNames);
		for (String indivFeatureName : featureNames) {
			dataProvStringBuilder.append(dataPath + indivFeatureName + ",");
			if (indivFeatureName.toLowerCase().contains(module.toLowerCase())) {
				break;
			}
		}
		return dataProvStringBuilder.toString();
	}

	public static String moduleFeatures(String module) {
		StringBuilder moStringBuilder = new StringBuilder();
		String[] featureNames = new File(modulesFolder).list();
		for (String indivFeatureName : featureNames) {
			if (indivFeatureName.toLowerCase().contains(module.toLowerCase())) {
				moStringBuilder.append(modulesPath + indivFeatureName);
				break;
			}
		}
		return moStringBuilder.toString();
	}

	private static Properties getSanityProperties(String module) {
		Properties regProp = new Properties();
		String dynamic = createDynamicFilterTags(module, "Sanity");
		String add = (dynamic.substring(0, dynamic.length() - 3).trim());
		regProp.put("cucumber.filter.tags", "@DataProvider or " + add);
		regProp.put("cucumber.features", dataProvFeatures(module) + modulesPath);
		return regProp;
	}

	private static String createDynamicFilterTags(String module, String type) {
		StringBuilder moStringBuilder = new StringBuilder();
		String[] moduleArr = module.split(",");
		for (String s : moduleArr) {
			moStringBuilder.append("(@" + type + " and @" + s.trim() + ") or ");
		}
		return moStringBuilder.toString();
	}

	private static Properties getRegressionProperties(String module) {
		Properties regProp = new Properties();
		if (!module.equalsIgnoreCase("All")) {
			String dynamic = createDynamicFilterTags(module, "Regression");
			String add = (dynamic.substring(0, dynamic.length() - 3).trim());
			regProp.put("cucumber.filter.tags", "@DataProvider or " + add);
			regProp.put("cucumber.features", dataProvFeatures(module) + modulesPath);
		} else {
			regProp.put("cucumber.filter.tags", "@DataProvider or @Regression");
			regProp.put("cucumber.features",
					"src/test/resources/features/dataProvider,src/test/resources/features/modules");
		}
		return regProp;
	}

	private static Properties getDeleteProperties(String module) {
		Properties regProp = new Properties();
		String dynamic = createDynamicFilterTags(module.replace(" ", ""), "Delete");
		regProp.put("cucumber.filter.tags", dynamic.substring(0, dynamic.length() - 3).trim());
		regProp.put("cucumber.features", "src/test/resources/features/modules");
		return regProp;
	}

	private static Properties getSmokeProperties() {
		Properties regProp = new Properties();
		regProp.put("cucumber.filter.tags", "@Smoke");
		regProp.put("cucumber.features",
				"src/test/resources/features/dataProvider,src/test/resources/features/modules");
		return regProp;
	}
}
