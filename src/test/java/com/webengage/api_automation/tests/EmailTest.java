package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.CampaignModuleUtility;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class EmailTest {

	CommonTest commonTest = new CommonTest();
	CampaignModuleUtility campModule = new CampaignModuleUtility();
	SegmentModuleUtility segmentModule = new SegmentModuleUtility();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		commonTest.initialize();
	}

	@Test(dataProvider = "EmailData", dataProviderClass = PayLoadDP.class)
	public void testEmail(HashMap<Object, String> emailMap) throws IOException, ParseException {
		SegmentModuleUtility.runtimeURL.clear();
		String campaignId = emailMap.get("id");
		commonTest.updateDynamicUrl("CampaignId", campaignId);
		commonTest.fetchCampaignResponse("fetchEmailCampaignUsers", "SMSCampaigns", "fetchUserStats", true);
		commonTest.verifyUsers(emailMap.get("user_id").split(";"), "response.data.stats[i].user_id");
		commonTest.fetchCampaignResponse("emailAggregates", "SMSCampaigns", "fetchStats", false);
		campModule.fetchStatsList("response.data[0].dimensions[0].metrics", commonTest.setStatsMap(emailMap));
	}

	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}
