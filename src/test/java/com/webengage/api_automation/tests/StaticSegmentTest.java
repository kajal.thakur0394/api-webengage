package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class StaticSegmentTest {
	CommonTest commonTest = new CommonTest();
	SegmentModuleUtility segmentModule = new SegmentModuleUtility();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		System.out.println("Running Static Segments");
		commonTest.initialize();
	}

	@Test(dataProvider = "StaticSegmentsData", dataProviderClass = PayLoadDP.class)
	public void testSegments(HashMap<Object, String> segMap) throws IOException, ParseException {
		SegmentModuleUtility.runtimeURL.clear();
		System.out.println(segMap.get("id") + " " + segMap.get("scenario_name") + " " + segMap.get("user_id"));
		String segId = segMap.get("id");
		commonTest.updateDynamicUrl("SegmentId", segId);
		String users[] = segMap.get("user_id").split(";");
		Assert.assertTrue(users.length > 0);	
		commonTest.verifyUsersInSegmentResponse("fetchStaticSegmentsUser");
		commonTest.verifyTotalUser("response.data.stats.total",users);
	
	}

	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}