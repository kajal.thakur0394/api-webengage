package com.webengage.api_automation.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.simple.parser.ParseException;

import com.webengage.api_automation.helperclasses.ExcelReader;
import com.webengage.api_automation.utilities.CampaignModuleUtility;
import com.webengage.api_automation.utilities.CommonUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;
import com.webengage.api_automation.utilities.SetupUtility;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class CommonTest {

	SegmentModuleUtility segmentModule = new SegmentModuleUtility();
	CommonUtility commonUtility = new CommonUtility();
	ExcelReader excelReader = new ExcelReader();
	SetupUtility setupUtility = new SetupUtility();
	CampaignModuleUtility campModule = new CampaignModuleUtility();

	public void initialize() throws IOException, ParseException {
		System.out.println("Initialization");
		String namespace = System.getProperty("set.Namespace");
		namespace = namespace.replace("-", "").trim();
		System.setProperty("set.Namespace", namespace);
		System.out.println("Execution Tag->" + System.getProperty("set.ExecutionTag"));
		System.out.println("Environment->" + System.getProperty("set.Environment"));
		System.out.println("BuildNumber->" + System.getProperty("set.BuildNumber"));
		System.out.println("AccountName->" + System.getProperty("set.AccountName"));
		System.out.println("Namespace->" + System.getProperty("set.Namespace"));
		String env = System.getProperty("set.Environment");
		System.setProperty("set.Environment", env);
		ExcelReader.flushRuntimeParams();
		SetupUtility setupUtility = new SetupUtility();
		setupUtility.loadCredentials(System.getProperty("set.AccountName"));
	}

	public void fetchCampaignResponse(String uri, String sheet, String refCol, Boolean appendDate) throws IOException {
		setupUtility.setApiURI(uri);
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheet);
		HashMap<String, String> apiParams = excelReader.processExcelSheetData(sheetObj, refCol);
		if (appendDate) {
			segmentModule.appendFromAndToDate();
			segmentModule.setDynamicRequest("from", "Query");
			segmentModule.setDynamicRequest("to", "Query");
		}
		String queryParamsString = apiParams.get("QueryParams");
		queryParamsString = segmentModule.modifyQueryParams(queryParamsString);
		List<List<String>> queryParams = commonUtility.generateQueryParams(queryParamsString);
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		System.out.println(apiURI);
		commonUtility.getRequest(apiURI, queryParams);
	}

	public void verifyUsersInSegmentResponse(String uri) throws IOException {
		setupUtility.setApiURI(uri);
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		System.out.println(apiURI);
		commonUtility.getRequest(apiURI);
	}

	public void verifyUsers(String[] users, String jpath) {
		for (String user : users)
			commonUtility.verifyValuefromArray(jpath, user);
	}

	public void verifyTotalUser(String jpath, String[] users) {
		commonUtility.verifyTotalUser(jpath, users);
	}
	
	public void verifyTotalUserCount(String uri, String jpath, String[] users) throws IOException {
		setupUtility.setApiURI(uri);
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		System.out.println(apiURI);
		commonUtility.getRequest(apiURI);
		commonUtility.verifyTotalUserCount(jpath, users);
	}

	public HashMap<String, String> setStatsMap(HashMap<Object, String> dataMap) {
		String userSize = Integer.toString(dataMap.get("user_id").split(";").length);
		HashMap<String, String> paraMap = new HashMap<String, String>();
		paraMap.put("sent", userSize);
		paraMap.put("deliveries", userSize);
		return paraMap;
	}

	public void updateDynamicUrl(String valueToReplace, String id) {
		CommonUtility.runtimeValues.put(valueToReplace, id);
		segmentModule.setDynamicRequest(valueToReplace, "URL");
	}

	public void fetchJourneyResponse(String uri) throws IOException {
		setupUtility.setApiURI(uri);
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		System.out.println(apiURI);
		commonUtility.getRequest(apiURI);
	}

	public HashMap<String, HashMap<String, String>> extractActualStats() throws ParseException, IOException {
		HashMap<String, HashMap<String, String>> ids = new HashMap<>();
		List<Object> objList = commonUtility.fetchObjectList("response.data.contexts[0].triggerSets");
		for (Object obj : objList) {
			HashMap<String, String> value = new HashMap<>();
			JSONObject jo = getJsonObject(new Gson().toJson(obj));
			value.put("exited", jo.get("exited").toString());
			ids.put(jo.get("id").toString(), value);
		}
		objList = commonUtility.fetchObjectList("response.data.contexts[0].states");
		for (Object obj : objList) {
			HashMap<String, String> value = new HashMap<>();
			JSONObject jo = getJsonObject(new Gson().toJson(obj));
			value.put("entered", jo.get("entered").toString());
			value.put("exited", jo.get("exited").toString());
			ids.put(jo.get("id").toString(), value);
		}
		fetchJourneyResponse("journeyStats");
		String overallEnteredCount = commonUtility.fetchJpathValue("response.data.contents[0].stats.entered");
		String overallExitedCount = commonUtility.fetchJpathValue("response.data.contents[0].stats.exited");
		HashMap<String, String> overallStats = new HashMap<>();
		overallStats.put("OverallEnteredCount", overallEnteredCount);
		overallStats.put("OverallExitCount", overallExitedCount);
		ids.put("OverallStats", overallStats);
		return ids;
	}

	public JSONObject getJsonObject(String data) throws ParseException {
		Object obj = new JSONParser().parse(data);
		return (JSONObject) obj;

	}

	public void validateUserStats(String expectedStats, String userId) throws ParseException, IOException {
		assertEquals(commonUtility.fetchJpathValue("response.data.userId"), userId);
		HashMap<String, HashMap<String, String>> actualStatsMap = extractActualStats();
		HashMap<String, HashMap<String, String>> expectedStatsMap = convertJsonToHashMap(expectedStats);
		boolean validation = expectedStatsMap.entrySet().containsAll(actualStatsMap.entrySet());
		assertTrue(validation, "actual stats data not found in expected stats");
	}

	public HashMap<String, HashMap<String, String>> convertJsonToHashMap(String json) {
		ObjectMapper objmap = new ObjectMapper();
		try {
			return objmap.readValue(json, new TypeReference<HashMap<String, HashMap<String, String>>>() {
			});
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void deactivateJourney() throws IOException {
		setupUtility.setApiURI("deactivateJourney");
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		commonUtility.postRequest(apiURI);
	}
}