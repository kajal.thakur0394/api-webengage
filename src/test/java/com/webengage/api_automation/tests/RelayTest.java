package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class RelayTest {
	CommonTest commonTest = new CommonTest();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		System.out.println("Running Relays");
		commonTest.initialize();
	}

	@Test(dataProvider = "RelayData", dataProviderClass = PayLoadDP.class)
	public void testJourneys(HashMap<Object, String> relayMap) throws ParseException, IOException {
		SegmentModuleUtility.runtimeURL.clear();
		String relayId = relayMap.get("id");
		String userId = relayMap.get("user_id");
		commonTest.updateDynamicUrl("journeyId", relayId);
		commonTest.updateDynamicUrl("userId", userId);
		commonTest.updateDynamicUrl("journeyType", "RELAY");
		commonTest.fetchJourneyResponse("userStatsJourney");
		commonTest.validateUserStats(relayMap.get("expected_stats"), userId);
	}
	
	@AfterMethod
	public void deactivateJourneys() throws IOException {
		commonTest.deactivateJourney();
	}
	
	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}
