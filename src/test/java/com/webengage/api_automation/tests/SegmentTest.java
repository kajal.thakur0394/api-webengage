package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class SegmentTest {
	CommonTest commonTest = new CommonTest();
	SegmentModuleUtility segmentModule = new SegmentModuleUtility();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		System.out.println("Running Live Segments");
		commonTest.initialize();
	}

	@Test(dataProvider = "SegmentsData", dataProviderClass = PayLoadDP.class)
	public void testSegments(HashMap<Object, String> segMap) throws IOException, ParseException {
		SegmentModuleUtility.runtimeURL.clear();
		System.out.println(segMap.get("id") + " " + segMap.get("scenario_name") + " " + segMap.get("user_id"));
		String segId = segMap.get("id");
		commonTest.updateDynamicUrl("SegmentId", segId);
		String users[]=segMap.get("user_id").split(";");
		if(segMap.get("scenario_name").contains("count of users")) {
			commonTest.verifyTotalUserCount("fetchLiveSegmentsTotalUsersCount","response.data.totalCount",users);
		}
		for(String user:users) {
		commonTest.updateDynamicUrl("userId", user);
		commonTest.verifyUsersInSegmentResponse("fetchLiveSegmentsUser");
		commonTest.verifyUsers(new String[] {user}, "response.data.contents[i].cuid");
		}		
	}

	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}