package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class JourneyTest {
	CommonTest commonTest = new CommonTest();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		System.out.println("Running Journeys");
		commonTest.initialize();
	}

	@Test(dataProvider = "JourneyData", dataProviderClass = PayLoadDP.class)
	public void testJourneys(HashMap<Object, String> journeyMap) throws ParseException, IOException {
		SegmentModuleUtility.runtimeURL.clear();
		String journeyId = journeyMap.get("id");
		String userId = journeyMap.get("user_id");
		commonTest.updateDynamicUrl("journeyId", journeyId);
		commonTest.updateDynamicUrl("userId", userId);
		commonTest.updateDynamicUrl("journeyType", "JOURNEY");
		commonTest.fetchJourneyResponse("userStatsJourney");
		commonTest.validateUserStats(journeyMap.get("expected_stats"), userId);
	}
	
	@AfterMethod
	public void deactivateJourneys() throws IOException {
		commonTest.deactivateJourney();
	}
	
	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}
