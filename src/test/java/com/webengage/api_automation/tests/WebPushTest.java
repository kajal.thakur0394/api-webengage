package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.CampaignModuleUtility;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class WebPushTest {
	CampaignModuleUtility campModule = new CampaignModuleUtility();
	CommonTest commonTest = new CommonTest();
	SegmentModuleUtility segmentModule = new SegmentModuleUtility();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		System.out.println("Running WebPush");
		commonTest.initialize();
	}

	@Test(dataProvider = "WebPushData", dataProviderClass = PayLoadDP.class)
	public void testWP(HashMap<Object, String> webPushMap) throws IOException, ParseException {
		SegmentModuleUtility.runtimeURL.clear();
		String campaignId = webPushMap.get("id");
		commonTest.updateDynamicUrl("CampaignId", campaignId);
		commonTest.fetchCampaignResponse("fetchWebPushCampaignUsers", "SMSCampaigns", "fetchUserStats", true);
		commonTest.verifyUsers(webPushMap.get("user_id").split(";"), "response.data.stats[i].user_id");
		commonTest.fetchCampaignResponse("webPushAggregates", "SMSCampaigns", "fetchStats", false);
		campModule.fetchStatsList("response.data[0].dimensions[0].metrics", commonTest.setStatsMap(webPushMap));
	}

	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}
