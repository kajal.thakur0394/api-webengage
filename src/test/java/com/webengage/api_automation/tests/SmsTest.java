package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.CampaignModuleUtility;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class SmsTest {
	CampaignModuleUtility campModule = new CampaignModuleUtility();
	CommonTest commonTest = new CommonTest();
	SegmentModuleUtility segmentModule = new SegmentModuleUtility();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		System.out.println("Running SMS");
		commonTest.initialize();
	}

	@Test(dataProvider = "SmsData", dataProviderClass = PayLoadDP.class)
	public void testSMS(HashMap<Object, String> smsMap) throws IOException, ParseException {
		SegmentModuleUtility.runtimeURL.clear();
		String campaignId = smsMap.get("id");
		commonTest.updateDynamicUrl("CampaignId", campaignId);
		commonTest.fetchCampaignResponse("fetchSMSCampaignUsers", "SMSCampaigns", "fetchUserStats", true);
		commonTest.verifyUsers(smsMap.get("user_id").split(";"), "response.data.stats[i].user_id");
		commonTest.fetchCampaignResponse("smsAggregates", "SMSCampaigns", "fetchStats", false);
		campModule.fetchStatsList("response.data[0].dimensions[0].metrics", commonTest.setStatsMap(smsMap));
	}

	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}
