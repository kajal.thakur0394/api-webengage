package com.webengage.api_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.webengage.api_automation.dataProviders.PayLoadDP;
import com.webengage.api_automation.utilities.CampaignModuleUtility;
import com.webengage.api_automation.utilities.DatabaseUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;

public class WhatsappTest {
	CampaignModuleUtility campModule = new CampaignModuleUtility();
	CommonTest commonTest = new CommonTest();
	SegmentModuleUtility segmentModule = new SegmentModuleUtility();

	@BeforeTest
	public void initialize() throws IOException, ParseException {
		System.out.println("Running WP");
		commonTest.initialize();
	}

	@Test(dataProvider = "WhatsappData", dataProviderClass = PayLoadDP.class)
	public void testWP(HashMap<Object, String> whatsappMap) throws IOException, ParseException {
		SegmentModuleUtility.runtimeURL.clear();
		String campaignId = whatsappMap.get("id");
		commonTest.updateDynamicUrl("CampaignId", campaignId);
		commonTest.fetchCampaignResponse("fetchWhatsappCampaignUsers", "SMSCampaigns", "fetchUserStats", true);
		commonTest.verifyUsers(whatsappMap.get("user_id").split(";"), "response.data.stats[i].user_id");
		commonTest.fetchCampaignResponse("whatsappAggregates", "SMSCampaigns", "fetchStats", false);
		campModule.fetchStatsList("response.data[0].dimensions[0].metrics", commonTest.setStatsMap(whatsappMap));
	}

	@AfterSuite
	public void tearDown() {
		System.out.println("Connection Closed");
		DatabaseUtility.closeDataSources();
	}
}
