package com.webengage.api_automation.stepDefinitions;

import com.webengage.api_automation.utilities.CommonUtility;

import io.cucumber.java.en.Then;

public class IntegrationStepDefinition {
	CommonUtility commonUtility = new CommonUtility();

	@Then("search and verify from {string} array with value as {string} from saved runtime values")
	public void search_and_verify_from_array_with_value_as_from_saved_runtime_values(String jsonPath,
			String runtimeKey) {
		commonUtility.checkIfPresent(jsonPath, CommonUtility.runtimeValues.get(runtimeKey));
	}

	@Then("I verify the value of {string} as {string}")
	public void i_verify_the_value_of_as(String identifier, String value) {
		commonUtility.verifyFromJsonObj(identifier, value);
	}

}
