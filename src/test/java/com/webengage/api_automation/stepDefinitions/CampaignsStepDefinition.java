package com.webengage.api_automation.stepDefinitions;

import java.io.IOException;

import com.webengage.api_automation.utilities.CampaignModuleUtility;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CampaignsStepDefinition {

	@Steps
	CampaignModuleUtility campaignModule;

	@Then("I use multiple values of {string} for dynamic parameterized segment body")
	public void i_use_multiple_value_of_for_dynamic_body(String values) throws IOException {
		campaignModule.setDynamicRequestBody(values);
	}

	@Then("I use multiple values of {string} for dynamic parameterized ssp body")
	public void i_use_multiple_value_of_for_dynamic_ssp_body(String values) throws IOException {
		campaignModule.setDynamicRequestBodyforSSP(values);
	}

	@Then("I select {string} for Android and {string} for iOS Configs")
	public void i_select_for_android_and_for_i_os_configs(String packageName, String bundleName) throws IOException {
		bundleName=System.getProperty("set.Environment").contains("Prod")?bundleName.replace("{env}", "prod"):bundleName.replace("{env}", "stage");
		campaignModule.setAppIds(packageName,bundleName);
	}
	
	@Then("I set the Epoch time as {string}")
	public void i_set_the_epoch_time_as(String time) {
		campaignModule.setEpochTime(time);
		}

	@And("I get Credential and WSP Id for template {string}")
	public void i_get_credential_and_wsp_id_for_template(String templateName) {
		campaignModule.setTemplateCredential(templateName);
	}
}
