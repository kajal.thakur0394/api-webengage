package com.webengage.api_automation.stepDefinitions;

import java.io.IOException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import com.webengage.api_automation.utilities.DeleteUtility;

public class DeleteStepDefinitions {

	@Steps
	DeleteUtility deleteUtility;

	@And("I fetch all the segment Ids")
	public void i_fetch_all_the_segment_ids() throws IOException {
		deleteUtility.fetchAllIds("trafficSegments","id");
	}
	
	@And("I fetch all the static lists Ids")
	public void i_fetch_all_the_static_list_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchStaticSegments","encodedId");
	}

	@And("I delete all the Segments on Live Segments page")
	public void i_want_to_delete_all_the_segments_on_live_segments_page() throws IOException {
		deleteUtility.deleteAllfromList("deleteSegment","{segId}");
	}
	
	@And("I delete all the Segments on Static Lists page")
	public void i_want_to_delete_all_the_segments_on_static_lists_page() throws IOException {
		deleteUtility.deleteAllfromList("deleteStaticList","{staticListId}");
	}

	@And("fetch all the SP Ids for {string} channel")
	public void i_fetch_all_the_ssp_ids(String channel) throws IOException {
		deleteUtility.fetchAllSPId(String.valueOf(channel.charAt(0)).toUpperCase());
	}
	
	@And("delete all the filtered {string} SP")
	public void delete_all_the_SSP(String channelSP) throws IOException {
		deleteUtility.deleteFilteredSP(String.valueOf(channelSP.charAt(0)).toUpperCase());
	}
	
	@And("I fetch all the Journey Ids")
	public void i_fetch_all_the_journey_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchJourneyList","journeyEId");
	}
	
	@And("I fetch all the Relays Ids")
	public void i_fetch_all_the_relays_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchRelaysList","journeyEId");
	}
	
	@And("I fetch all the WA Campaign Ids")
	public void i_fetch_all_the_wa_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchWACampaigns","id");
	}
	
	@And("I fetch all the SMS Campaign Ids")
	public void i_fetch_all_the_sms_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchSMSCampaigns","id");
	}
	
	@And("I fetch all the Email Campaign Ids")
	public void i_fetch_all_the_email_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchEmailCampaigns","id");
	}
	
	@And("I fetch all the In Line Content Campaign Ids")
	public void i_fetch_all_the_in_line_content_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchInLineContentCampaigns","id");
	}
	
	@And("I fetch all the In Line Content App Campaign Ids")
	public void i_fetch_all_the_in_line_content_app_campaign_ids() throws IOException {
	    deleteUtility.fetchAllIds("fetchInLineContentAppCampaigns","id");
	}
	
	@And("I fetch all the Push Campaign Ids")
	public void i_fetch_all_the_push_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchPushCampaigns","id");
	}
	
	@And("I fetch all the In App Campaign Ids")
	public void i_fetch_all_the_in_app_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchInAppCampaigns","id");
	}
	
	@And("I fetch all the Viber Campaign Ids")
	public void i_fetch_all_the_viber_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchViberCampaigns","id");
	}
	
	@And("I fetch all the On Site Campaign Ids")
	public void i_fetch_all_the_on_site_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchOnSiteCampaigns","id");
	}
	
	@And("I fetch all the Web Push Campaign Ids")
	public void i_fetch_all_the_web_push_campaign_ids() throws IOException {
		deleteUtility.fetchAllIds("fetchWebPushCampaigns","id");
	}
	
	@And("I stop all the {string} and delete them")
	public void i_stop(String type) throws IOException {
		deleteUtility.deleteAllfromList("delete" +type,"{journeyId}");
	}
	
	@And("delete the filtered {string} campaigns")
	public void delete_filtered_campaigns(String type) throws IOException {
		deleteUtility.deleteAllfromList("delete"+type+"Campaigns","{campaignId}");
	}
	
	@Then("I exclude all the {string} defined in the exclude list")
	public void exclude_all_the_defined_in_the_exclude_list(String type) {
		deleteUtility.excludeFromListwithName(type.toLowerCase());
	}
	
	@And("fetch all the whatsapp template Ids")
	public void fetch_all_the_whatsapp_template_ids() throws IOException {
	    deleteUtility.fetchAllWSPTemplateIds("fetchWATemplate");
	}
	
	@Then("delete all the filtered whatsapp templates")
	public void delete_all_the_filtered_whatsapp_templates() throws IOException {
	    deleteUtility.deleteAllTemplatesFromList("deleteWATemplate");
	  
	}
	
	@And("I fetch all the Dashboard Ids")
	public void fetch_dashboard_id() throws IOException {
		deleteUtility.fetchAllIds("fetchDashboardsModule","dashboardEId");
	}

	@And("I fetch all the {string} module Ids")
    public void i_fetch_all_the_module_ids(String moduleName) throws IOException {
	    deleteUtility.fetchAllIds("fetch".concat(moduleName).concat("Module"),"slug");
	}

	@And("delete the filtered {string} modules")
	public void delete_the_filtered_modules(String moduleName) throws IOException {
	    deleteUtility.deleteAllfromList("delete".concat(moduleName).concat("Module"),"{slug}");
	}
	
	@And("I fetch all the custom alert Ids")
	public void fetch_custom_alert_id() throws IOException {
		deleteUtility.fetchAllIds("fetchCustomAlertsModule","id");
	}
	
	@And("I fetch all the funnels Ids")
	public void fetch_funnel_id() throws IOException {
		deleteUtility.fetchAllIds("fetchFunnelsModule", "id");
	}
	
	@And("I fetch all the custom template Ids")
	public void fetch_custom_template_id() throws IOException {
		deleteUtility.fetchAllIds("fetchInlineAppCustomTemplate","templateId");
	}
	
	@Then("delete all the filtered {string}")
	public void delete_filtered_custom_template_id(String type) throws IOException {
		deleteUtility.deleteAllfromList("delete"+type,"{templateID}");
	}

}