package com.webengage.api_automation.runners;

import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.webengage.api_automation.helperclasses.ExcelReader;
import com.webengage.api_automation.utilities.SetupUtility;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(features = {
//		"src/test/resources/features/dataProvider",
//		"src/test/resources/features/modules" 
},

		glue = { "com.webengage.api_automation.stepDefinitions", "com.webengage.api_automation.hooks" }, plugin = {
				"pretty", "json:target/cucumber-reports/Cucumber.json", "junit:target/cucumber-reports/Cucumber.xml", },

//		tags = "@Integration",

		monochrome = true)

public class TestRunner {
	@BeforeClass
	public static void initialize() throws IOException, ParseException {
		SetupUtility setupUtility=new SetupUtility();
		ExcelReader.flushRuntimeParams();
		setupUtility.loadCredentials(System.getProperty("set.Account").equals("null")?"APIAutomation":System.getProperty("set.Account"));
	}

	@AfterClass
	public static void tearDown() throws IOException {
		ExcelReader.writeRuntimeValues();
	}
}
