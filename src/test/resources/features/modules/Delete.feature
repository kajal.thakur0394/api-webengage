@Delete
Feature: Delete desired content
  Provision to delete individual things and all at once

  @DeleteJourneys
  Scenario: Delete all listed Journeys
    Given I exclude all the "Journeys" defined in the exclude list
    And I fetch all the Journey Ids
    And I stop all the "Journeys" and delete them

  @DeleteRelays
  Scenario: Delete all listed Relays
    Given I exclude all the "relays" defined in the exclude list
    And I fetch all the Relays Ids
    And I stop all the "Relays" and delete them

  @DeleteWACampaigns
  Scenario: Delete all listed WA Campaigns
    Given I exclude all the "whatsappCampaigns" defined in the exclude list
    And I fetch all the WA Campaign Ids
    And delete the filtered "WA" campaigns

  @DeleteSMSCampaigns
  Scenario: Delete all listed SMS Campaigns
    Given I exclude all the "smsCampaigns" defined in the exclude list
    And I fetch all the SMS Campaign Ids
    And delete the filtered "SMS" campaigns

  @DeleteEmailCampaigns
  Scenario: Delete all listed Email Campaigns
    Given I exclude all the "emailCampaigns" defined in the exclude list
    And I fetch all the Email Campaign Ids
    And delete the filtered "Email" campaigns

  @DeletePushCampaigns
  Scenario: Delete all listed Push Campaigns
    Given I exclude all the "pushCampaigns" defined in the exclude list
    And I fetch all the Push Campaign Ids
    And delete the filtered "Push" campaigns

  @DeleteWebPushCampaigns
  Scenario: Delete all listed Push Campaigns
    Given I exclude all the "webPushCampaign" defined in the exclude list
    And I fetch all the Web Push Campaign Ids
    And delete the filtered "WebPush" campaigns

  @DeleteInLineContent
  Scenario: Delete all listed InLineContent Campaigns
    Given I exclude all the "inLineContent" defined in the exclude list
    And I fetch all the In Line Content Campaign Ids
    And delete the filtered "inLineContent" campaigns

  @DeleteInLineAppContent
  Scenario: Delete all listed InLineAppContent Campaigns
    Given I exclude all the "inLineContentApp" defined in the exclude list
    And I fetch all the In Line Content App Campaign Ids
    And delete the filtered "inLineContentApp" campaigns

  @DeleteInAppContent
  Scenario: Delete all listed InApp Campaigns
    Given I exclude all the "inApp" defined in the exclude list
    And I fetch all the In App Campaign Ids
    And delete the filtered "inApp" campaigns

  @DeleteViberCampaigns
  Scenario: Delete all listed Viber Campaigns
    Given I exclude all the "viberCampaign" defined in the exclude list
    And I fetch all the Viber Campaign Ids
    And delete the filtered "Viber" campaigns

  @DeleteOnSiteNotif
  Scenario: Delete all listed InApp Campaigns
    Given I exclude all the "onSite" defined in the exclude list
    And I fetch all the On Site Campaign Ids
    And delete the filtered "onSiteNotif" campaigns

  @DeleteSegments
  Scenario: Delete all listed Segments
    Given I exclude all the "Segments" defined in the exclude list
    And I fetch all the segment Ids
    And I delete all the Segments on Live Segments page

  @DeleteStaticLists
  Scenario: Delete all listed Static lists
    Given I exclude all the "StaticLists" defined in the exclude list
    And I fetch all the static lists Ids
    And I delete all the Segments on Static Lists page

  @DeleteSSP
  Scenario: Delete all listed SSP
    Given I exclude all the "SSP" defined in the exclude list
    And fetch all the SP Ids for "SMS" channel
    Then delete all the filtered "SMS" SP

  @DeleteESP
  Scenario: Delete all listed ESP
    Given I exclude all the "ESP" defined in the exclude list
    And fetch all the SP Ids for "Email" channel
    Then delete all the filtered "Email" SP

  @DeleteWSP
  Scenario: Delete all listed WSP
    Given I exclude all the "WSP" defined in the exclude list
    And fetch all the SP Ids for "WA" channel
    Then delete all the filtered "WA" SP

  @DeleteWSPTemplates
  Scenario: Delete all listed WSP Templates
    Given I exclude all the "WATemplates" defined in the exclude list
    And fetch all the whatsapp template Ids
    Then delete all the filtered whatsapp templates

  @DeleteCatalogs
  Scenario: Delete all listed Catalogs
    Given I exclude all the "catalogs" defined in the exclude list
    And I fetch all the "Catalogs" module Ids
    And delete the filtered "Catalogs" modules

  @DeleteRecommendations
  Scenario: Delete all listed Recommendations
    Given I exclude all the "Recommendations" defined in the exclude list
    And I fetch all the "Recommendations" module Ids
    And delete the filtered "Recommendations" modules

  @DeleteDashboards
  Scenario: Delete all listed Dashboards
    Given I exclude all the "Dashboards" defined in the exclude list
    And I fetch all the Dashboard Ids
    And delete the filtered "Dashboards" modules

  @DeleteCustomAlerts
  Scenario: Delete all custom alerts
    Given I exclude all the "alerts" defined in the exclude list
    And I fetch all the custom alert Ids
    And delete the filtered "CustomAlerts" modules

  @DeleteFunnels
  Scenario: Delete all listed Funnels
    Given I exclude all the "funnels" defined in the exclude list
    And I fetch all the funnels Ids
    And delete the filtered "Funnels" modules

  @DeleteInLineAppCustomTemplates
  Scenario: Delete all InLineapp Custom Templates
    Given I exclude all the "InLineappCustomTemplate" defined in the exclude list
    And I fetch all the custom template Ids
    And delete all the filtered "InlineAppCustomTemplate"
